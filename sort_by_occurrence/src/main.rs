use lambda_runtime::{service_fn, LambdaEvent, Error, Context};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Deserialize, Serialize)]
struct Input {
    occurrences: HashMap<String, usize>,
}

#[derive(Deserialize, Serialize)]
struct Output {
    sorted_data: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(sort_by_occurrence);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn sort_by_occurrence(event: LambdaEvent<Input>) -> Result<Output, Error> {
    let (event, _context): (Input, Context) = event.into_parts();
    
    let mut items: Vec<_> = event.occurrences.iter().collect();
    items.sort_by(|a, b| a.1.cmp(b.1).then(a.0.cmp(b.0)));

    let sorted_data = items
        .into_iter()
        .flat_map(|(num, count)| std::iter::repeat(num).take(*count))
        .map(|s| s.to_string())
        .collect::<Vec<_>>()
        .join(",");
    
    Ok(Output { sorted_data })
}

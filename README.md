# yg229 Individual Project 4

This project demonstrates the creation and deployment of two AWS Lambda functions using Rust. The functions are part of a data processing pipeline orchestrated with AWS Step Functions. The first function, data_occurrence, calculates the occurrences of each number in a given string. The second function, sort_by_occurrence, sorts these numbers first by occurrence and then by value if the occurrences are the same.

## Demo Video
![](demo.mov)

## Features
- Rust-Based Lambda Functions: Leverages the power and safety of Rust for serverless computing.
- AWS Step Functions Integration: Uses AWS Step Functions to orchestrate the flow between multiple Lambda functions, ensuring a seamless data processing pipeline.
- Automated Deployment: Utilizes GitLab CI/CD pipelines for automated building, testing, and deploying of Lambda functions.
Scalable Architecture: Designed to handle varying loads efficiently with AWS Lambda's scalable infrastructure.

## Initial Setup
- Create New Rust Projects: Start by setting up the Rust projects for both Lambda functions.
1. ```cargo new data_occurrence```
2. ```cargo new sort_by_occurrence```
- Project Structure: Each project should have a Cargo.toml file where dependencies can be specified and a src directory for source files.
## Development
- Implement data_occurrence Function:
- Navigate to the data_occurrence directory.
- Implement the function logic in src/main.rs.
- Add necessary dependencies in Cargo.toml.
- Implement sort_by_occurrence Function:
- Navigate to the sort_by_occurrence directory.
- Implement the function logic in src/main.rs.
- Ensure to handle the sorting based on both occurrences and numerical order.

## Test Locally
1. On one terminal: ```cargo lambda watch```
2. On the other terminal: ```cargo lambda invoke --data-file ./input.json```
- Here are the results of the two lambda functions with input string: 
```
{
    "string_data": "1,2,5,3,4,4,4,3"
}
```
and
```
{
    "occurrences": {
        "1": 1,
        "2": 1,
        "3": 2,
        "4": 3,
        "5": 1
    }
}

```
- For data_occurence function:
![](1.png)

- For sort_by_occurence function:
![](2.png)

## Deploy AWS FUNCTIONS ON AWS Lambda
1. ```cargo lambda build --release```
2. ```cargo lambda deploy```

### Results with same event
- For data_occurence function:
![](3.png)

- For sort_by_occurence function:
![](4.png)

## Set Up AWS Lambda and Step Functions:
- Create Lambda functions in the AWS Management Console or via the AWS CLI.
- Configure a State Machine in AWS Step Functions to orchestrate the Lambda functions based on the outputs and the process flow.
![](5.png)

## Final results
- The results meet the expectation that the state machine works fine with corrent output:
![](6.png)

## # Auto Deploy By CI/CD

```stages:
  - build
  - test
  - deploy

variables:
  AWS_DEFAULT_REGION: "us-east-1"

# Build Jobs
build_data_occurrence:
  stage: build
  image: rust:latest
  script:
    - rustup default stable
    - cargo build --release --manifest-path data_occurrence/Cargo.toml
  only:
    - main

build_sort_by_occurrence:
  stage: build
  image: rust:latest
  script:
    - rustup default stable
    - cargo build --release --manifest-path sort_by_occurrence/Cargo.toml
  only:
    - main

# Test Jobs
test_data_occurrence:
  stage: test
  image: rust:latest
  script:
    - rustup default stable
    - cargo test --manifest-path data_occurrence/Cargo.toml
  only:
    - main

test_sort_by_occurrence:
  stage: test
  image: rust:latest
  script:
    - rustup default stable
    - cargo test --manifest-path sort_by_occurrence/Cargo.toml
  only:
    - main```